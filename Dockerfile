FROM    python:3.7

ENV     LANG C.UTF-8
ENV     PROJECTPATH=/home/wis/wis_avia
ENV     USER wis

RUN     set -x && apt-get -qq update && apt-get install -y --no-install-recommends\
        libpq-dev python3-dev git && apt-get purge -y --autoremove \
        && rm -rf /var/lib/apt/lists/*

RUN     useradd -m -d /home/${USER} ${USER} && chown -R ${USER} /home/${USER}

RUN     mkdir -p ${PROJECTPATH} \
        && mkdir -p /home/${USER}/{media,static}

COPY    ./requirements.txt ${PROJECTPATH}

RUN     pip install --upgrade pip\
        && pip install --no-cache-dir -r ${PROJECTPATH}/requirements.txt

WORKDIR ${PROJECTPATH}
USER    ${USER}
